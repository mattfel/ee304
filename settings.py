import numpy as np

# Decision functions
def fcn_left(x):
    food = x[0] * food_gain
    predator = x[1] * predator_gain
    wall = x[2] * wall_gain
    # Recommendation = [left, straight, right]
    recommendation = [food - predator - wall, 
                      no_action, 
                      predator + .5*wall,
                      backup_gain*predator]
    return recommendation

def fcn_right(x):
    food = x[0] * food_gain
    predator = x[1] * predator_gain
    wall = x[2] * wall_gain
    # Recommendation = [left, straight, right]
    recommendation = [predator - wall,
                      no_action, 
                      food - predator + wall,
                      backup_gain*predator]
    return recommendation
# Field setup
field_size = 1000
num_neurons = [128, 1024, 512, 256, 64]
creature_taus = [.001, .005, .01, .02, .05]
num_creatures = len(num_neurons)
dt = .5
wall_pad = 30

# Vehicle properties
creature_velocity = 10
creature_weight = 30 # starting weight
starved = 8 # weight that creature starves at
max_weight = 100 # maximum weight before getting capped
creature_color = (0.0, 0.0, 1.0, 1.0) # Live color
dead_color = (0.5, 0.5, 0.5, 1.0) # Dead color
decay_rate = 0.00001 # rate * spikes * weight = weight lost
respawn_time = 50 # Cycles to wait before respawn after being eaten
pred_prey_buffer = 2 # Weight buffer in deciding between prey and pred

# Antennae properties
antenna_color = 'k'
antenna_length = 180
antennae_separation = np.pi/6 # radians separating antennae
nodes_per_antenna = 4 # Number of sense points on the antenna

# Vehicle action parameters
angle_change = np.pi/40
food_gain = 1.8
predator_gain = 1
wall_gain = 1
backup_gain = 2
no_action = 0.35 # Value of no_action rec to basal ganglia 

# Food properties
food_color = (0.0, 0.5, 0.0, 1.0)
food_size = 15 # radius of food in image
food_calories = 5 # amount to add to creature radius when eaten
food_spawn_delay = 40 # Number of frames per food spawn
num_food = 80 # Maximum number of food
creature_efficiency = 50 # Percent of creature weight to add to self