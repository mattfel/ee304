"""
Preydator EE304 Project

author: Matthew Feldman, Jacob Baldwin
email: mattfel@stanford.edu, jtb5np@stanford.edu
quarter: Spring 2016
Please feel free to use and modify this, but keep the above information. Thanks!
"""

import numpy as np
from scipy.spatial.distance import pdist, squareform
from vehicle import Vehicle
from settings import *
from food import Food

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import scipy.integrate as integrate
import matplotlib.animation as animation
from tabulate import tabulate



# set up figure and animation
fig = plt.figure()
ax = plt.subplot2grid((4,3), (0,0), colspan = 2, rowspan = 4)
steering = plt.subplot2grid((4,3), (0,2), rowspan = 2)
sparkle1 = plt.subplot2grid((4,3), (2,2))
sparkle2 = plt.subplot2grid((4,3), (3,2))
plt.axis([0,field_size,0,field_size])
ax.axis('off')
# steering.axis('off')
# sparkle1.axis('off')
steering.set_ylim([0, num_creatures])
steering.set_xlim([-3, 4])
steering.set_title("Steering Decision Process")
steering.set_yticks([0.5])
ytix = []
ylab = []
for i in range(0, num_creatures):
	ytix.append(0.5+i)
	ylab.append("P%d (%d Neur)" % (i, num_neurons[i]))
steering.set_yticks(ytix)
steering.set_yticklabels(ylab)
steering.set_xticks([-1,0,1,2])
steering.set_xticklabels(["Left", "Straight", "Right", "Reverse"])
dim = int(np.sqrt(2*num_neurons[0])) + 1
sparkle1.set_ylim([-1,dim])
sparkle1.set_xlim([-1,dim])
sparkle1.set_xticks([])
sparkle1.set_yticks([])
sparkle1.set_title("P0 Sparkle")
dim = int(np.sqrt(2*num_neurons[1])) + 1
sparkle2.set_ylim([-1,dim])
sparkle2.set_xlim([-1,dim])
sparkle2.set_xticks([])
sparkle2.set_yticks([])
sparkle2.set_title("P1 Sparkle")
ax.set_aspect(1)
outer_rect = plt.Rectangle((0,0), field_size, field_size,
                     ec='k', lw=5, fc='none')
ax.add_patch(outer_rect)
inner_rect = plt.Rectangle((wall_pad,wall_pad), 
						    field_size-2*wall_pad, 
						    field_size-2*wall_pad,
                     		ec='k', lw=1, fc='none', 
                     		linestyle = 'dotted')
ax.add_patch(inner_rect)

# creatures is the array that holds all the information about
#    the critters and does all of the logic and computation
creatures = []
for i in range(0, num_creatures):
	init_pos = np.random.randint(wall_pad+2, field_size-2*wall_pad-2, size = 2)
	init_angle = np.random.uniform(0, 2*np.pi)
	creatures.append(Vehicle(init_pos, init_angle, creature_weight, i, num_neurons[i], creature_taus[i]))

# foods is the array that holds all the information about
#    the food and does all of the logic and computation
foods = []
for i in range(0, num_food):
	init_pos = np.random.randint(wall_pad, field_size-2*wall_pad, size = 2)
	foods.append(Food(init_pos, i))


# handle the drawing
creature_bodies = []
creature_antennae = []
food_dots = []
steerings = []
sparkles1, = [sparkle1.plot(creatures[0].get_sparkle()[0], creatures[0].get_sparkle()[1], 
					marker = 's', ms = 5, linestyle = 'None', c = 'k')]
sparkles2, = [sparkle2.plot(creatures[1].get_sparkle()[0], creatures[0].get_sparkle()[1], 
					marker = 's', ms = 5, linestyle = 'None', c = 'k')]
for i in range(0, num_creatures):
	creature_bodies.append(creatures[i].get_body())
	ax.add_patch(creature_bodies[i])
	temp, = ax.plot(creatures[i].get_antenna_line()[0], creatures[i].get_antenna_line()[1], 
					 color = antenna_color, marker = '.', ms = 5)
	creature_antennae.append(temp)
	temp, = steering.plot([0, 0], [i+0.2, i+1], c= creatures[i].color)
	steerings.append(temp)
	temp, = steering.plot([-1, 0, 1, 2], creatures[i].node_stimuli, c= creatures[i].color)
	steerings.append(temp)
for i in range(0, num_food):
	food_dots.append(foods[i].get_body())
	ax.add_patch(food_dots[i])


def init():
	sparkles1[0].set_visible(False)
	sparkles2[0].set_visible(False)
	for i in range(0, num_creatures):
		creature_bodies[i].set_visible(False)
		steerings[i].set_visible(False)
		creature_antennae[i].set_visible(False)
	for i in range(0, num_food):
		food_dots[i].set_visible(False)
	return creature_bodies + creature_antennae + food_dots + steerings + sparkles1 + sparkles2

def animate(t):
	"""perform animation step"""
	# Let creatures think
	for i in range(0, num_creatures):
		creatures[i].update(creatures, foods)
	# Physically update all creatures
	for i in range(0, num_creatures):
		creatures[i].move(dt)
	# Physically update all food
	for i in range(0, num_food):
		foods[i].set_exists(t)

	# Redraw sparkle
	if t == 1:
		sparkles1[0].set_visible(True)
		sparkles2[0].set_visible(True)
	sparkles1[0].set_data(creatures[0].get_sparkle()[0], creatures[0].get_sparkle()[1])
	sparkles2[0].set_data(creatures[1].get_sparkle()[0], creatures[1].get_sparkle()[1])

	# Redraw creatures and legends
	for i in range(0, num_creatures):
		if t == 1:
			creature_bodies[i].set_visible(True)
			creature_antennae[i].set_visible(True)
			steerings[i].set_visible(True)
		creature_bodies[i].center = (creatures[i].pos[0], creatures[i].pos[1])
		creature_bodies[i].radius = creatures[i].weight
		creature_bodies[i]._facecolor = creatures[i].color
		creature_antennae[i].set_data(creatures[i].get_antenna_line())
		steerings[2*i].set_xdata([creatures[i].bg_decision, creatures[i].bg_decision])
		steerings[2*i+1].set_ydata(np.add(creatures[i].node_stimuli/nodes_per_antenna**2, i+.2))
		if creatures[i].antenna_fix == 1:
			creature_antennae[i].set_visible(True)			
			creatures[i].antenna_fix = 0
		if creatures[i].respawning == True:
			creature_bodies[i].set_visible(True)
			creatures[i].antenna_fix = 1
		elif creatures[i].eaten == True:
			creature_bodies[i].set_visible(False)
			creature_antennae[i].set_visible(False)
			
	# Redraw food
	for i in range(0, num_food):
		if foods[i].exists == True and foods[i].eaten == False:
			food_dots[i].set_visible(True)
		else:
			food_dots[i].set_visible(False)

	return creature_bodies + creature_antennae + food_dots + steerings + sparkles1 + sparkles2

ani = animation.FuncAnimation(fig, animate, frames=10000,
						interval=dt, blit=True, init_func=init)

mng = plt.get_current_fig_manager()
mng.resize(*mng.window.maxsize())
fig.show()

# Show results
table = []
for i in range(0, num_creatures):
	table.append(["Creature %i" % i, 
				  "%i" % num_neurons[i],
				  "%f" % creature_taus[i],
				  "%i" % creatures[i].live_time, 
				  "%i" % creatures[i].eat_count,
				  "%i" % creatures[i].eaten_count,
				  "%i" % creatures[i].starved_count])
key = ['ID', 'Neurons', 'Synapse Tau', 'Survival Time', 'Calories Consumed', 'Times Eaten', 'Times Starved']
print tabulate(table, headers=key)
