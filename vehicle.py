import numpy as np
import matplotlib.pyplot as plt
from settings import *
import nengo
from nengo.neurons import LIF, NeuronTypeParam, Direct, LIFRate

class Vehicle:
    """
    Class for all of the creatures
    """
    def __init__(self,
                 init_pos,
                 init_angle,
                 init_weight,
                 index,
                 my_neurons = num_neurons[0],
                 tau = .005
                 ):
        # Setup
        self.pos = np.asarray(init_pos, dtype=float)
        self.angle = np.asarray(init_angle, dtype=float)
        self.weight = init_weight
        self.vel = creature_velocity
        self.color = tuple([ a + b for a, b in zip(list(creature_color),[float(index)/float(num_creatures), 0.0, -float(index)/float(num_creatures), 0.0])])
        self.id = index
        self.left_tips, self.right_tips = self.update_tips()
        self.respawn_count = 0
        self.starved = False
        self.eaten = False
        self.respawning = False
        self.action = 'Wander'
        self.action_timer = 0
        self.forward = 1
        self.model = nengo.Network()
        self.decoded_output = 0
        self.current_input = []
        self.node_stimuli = [0,0,0,0]
        self.my_neurons = my_neurons
        self.tau = tau
        self.eaten_count = 0
        self.eat_count = 0
        self.live_time = 0
        self.starved_count = 0
        self.antenna_fix = 0


   #          ARCHITECTURE
   #                   (sum all nodes per antenna)
   #              o (food)  --
   # (left nodes) o (pred)  --  O (left antenna)    (weighting fcns)
   #              o (wall)  --                    \
   #                                                 O (basal ganglia) -- output decision
   #              o (food)  --                    /
   #(right nodes) o (pred)  --  O (right antenna)
   #              o (wall)  --

        # Initialize nodes
        for i in range(0, nodes_per_antenna*2):
            self.current_input.append(np.zeros(3))

        with self.model:
            # Make ensembles and basal ganglia
            self.coded_input = []
            for i in range(0, nodes_per_antenna*2):
                self.coded_input.append(nengo.Node(self.current_input[i]))
            self.antenna_left = nengo.Ensemble(
                                    n_neurons=self.my_neurons, 
                                    dimensions=3, 
                                    radius = nodes_per_antenna,
                                    neuron_type = LIF())
            self.antenna_right = nengo.Ensemble(
                                    n_neurons=self.my_neurons, 
                                    dimensions=3, 
                                    radius = nodes_per_antenna,
                                    neuron_type = LIF())
            self.bg = nengo.networks.BasalGanglia(dimensions=4)
            self.out_node = nengo.Node(size_in = 4)

            # Make connections
            for i in range(0, nodes_per_antenna):
                nengo.Connection(self.coded_input[i], self.antenna_left)
                nengo.Connection(self.coded_input[nodes_per_antenna + i], self.antenna_right)
            nengo.Connection(self.antenna_left, self.bg.input, function = fcn_left, synapse=self.tau)
            nengo.Connection(self.antenna_right, self.bg.input, function = fcn_right, synapse=self.tau)
            nengo.Connection(self.antenna_left, self.out_node, function = fcn_left)
            nengo.Connection(self.antenna_right, self.out_node, function = fcn_right)

            # Make probes
            self.decoded_node = nengo.Probe(self.out_node)
            self.decoded_bg = nengo.Probe(self.bg.output)

        self.sim = nengo.Simulator(self.model)

    #########################
    # NEUROMORPHIC BACK-END #
    #########################


    def update(self, creatures, foods):
        """Recompute direction, neuromorphic brain goes here"""
        # Decide if we are alive
        self.respawning = False
        if self.respawn_count > 0:
            self.respawn()
            return
        elif self.starved == True:
            self.color = dead_color
            self.vel = 0
            return
        elif self.starved == False:
            self.live_time += 1
            stimuli = []
            for i in range(0, nodes_per_antenna):
                # Sense left nodes
                stimuli.append(self.sense_objects(creatures, foods, i, "left"))
            for i in range(0, nodes_per_antenna):
                # Sense left nodes
                stimuli.append(self.sense_objects(creatures, foods, i, "right"))

        # Step
        self.sim.step()
        for i in range(0, nodes_per_antenna):
            self.coded_input[i].output = stimuli[i]
            self.coded_input[nodes_per_antenna+i].output = stimuli[nodes_per_antenna+i]

        # Count spikes
        total_spikes = 0
        for ensemble in self.model.ensembles:
            total_spikes += np.sum(self.sim.signals[self.sim.model.sig[ensemble]['out']])/1000.0
        # print 'Current # of Spikes: ' + str(total_spikes)

        # Feed stimuli
        for i in range(0, nodes_per_antenna*2):
            self.sim.signals[self.sim.model.sig[self.coded_input[i]]['out']] = stimuli[i]
        # print 'Current Stimuli: ',
        # print self.sim.signals[self.sim.model.sig[self.coded_input]['out']]

        # Probes
        self.bg_decision = self.sim.signals[self.sim.model.sig[self.decoded_bg]['in']].copy().argmax() - 1
        self.node_stimuli =  self.sim.signals[self.sim.model.sig[self.decoded_node]['in']].copy()
        self.node_decision = self.node_stimuli.argmax() - 1

        # Do physical updates
        if self.bg_decision == 2:
            self.forward = -1
        else:
            self.forward = 1
        self.angle += -self.bg_decision * angle_change
        self.left_tips, self.right_tips = self.update_tips()
        if self.starved == False and self.eaten == False:
            self.devour(creatures, foods)
            self.update_weight(total_spikes)

    def sense_objects(self, creatures, foods, nodeId, side):
        # TODO: Doesn't necessarily self.weight + pred_prey_bufferneed to be hierarchical when done neuromorphically
        # Sense prey/predator

        # 1D array stores [food, escape, wall]
        sense = [0,0,0]
        for i in range(0, num_creatures):
            if i != self.id:
                # creatures
                if side == "left":
                    left_dist = np.linalg.norm(self.left_tips[nodeId] - creatures[i].pos)
                    if (left_dist < creatures[i].weight and not creatures[i].eaten):
                        # Left prey
                        if (creatures[i].weight + pred_prey_buffer) < self.weight:
                            sense = np.add(sense, [1,0,0])
                        # Left predator
                        elif creatures[i].weight > (self.weight + pred_prey_buffer):
                            sense = np.add(sense, [0,1,0])
                elif side == "right":
                    right_dist = np.linalg.norm(self.right_tips[nodeId] - creatures[i].pos)
                    if (right_dist < creatures[i].weight and not creatures[i].eaten):
                        # Right prey
                        if (creatures[i].weight + pred_prey_buffer) < self.weight:
                            sense = np.add(sense, [1,0,0])                            
                        # Right predator
                        elif creatures[i].weight > (self.weight + pred_prey_buffer):
                            sense = np.add(sense, [0,1,0])
        for i in range(0, num_food):
            # food
            if side == "left":
                left_dist = np.linalg.norm(self.left_tips[nodeId] - foods[i].pos)
                if (left_dist < food_size and foods[i].exists and not foods[i].eaten):
                    # Left food
                    sense = np.add(sense, [1,0,0])                            
            elif side == "right":
                right_dist = np.linalg.norm(self.right_tips[nodeId] - foods[i].pos)
                if (right_dist < food_size and foods[i].exists and not foods[i].eaten):
                    # Right food
                    sense = np.add(sense, [1,0,0])                            

        if side == "left":
            if  self.left_tips[nodeId][0] < wall_pad or \
                    self.left_tips[nodeId][0] > field_size - wall_pad or \
                    self.left_tips[nodeId][1] < wall_pad or \
                    self.left_tips[nodeId][1] > field_size - wall_pad:
                        sense = np.add(sense, [0,0,1])                            
        elif side == "right":
            if  self.right_tips[nodeId][0] < wall_pad or \
                    self.right_tips[nodeId][0] > field_size - wall_pad or \
                    self.right_tips[nodeId][1] < wall_pad or \
                    self.right_tips[nodeId][1] > field_size - wall_pad:
                        sense = np.add(sense, [0,0,1])

        return sense



    def update_weight(self, total_spikes):
        """ Compute energy spent and shrink weight accordingly """
        self.weight -= decay_rate * total_spikes * self.weight
        if self.weight < starved:
            self.starved_count += 1
            self.starved = True



    ##################
    # PHYSICS ENGINE #
    ##################

    def move(self, dt):
        """step once by dt seconds"""
        dx = np.cos(self.angle) * self.vel * dt
        dy = np.sin(self.angle) * self.vel * dt

        # Check if at wall and do not let it go further
        real_dx, real_dy = self.wall(dx, dy)

        self.pos = self.pos + [self.forward*real_dx, self.forward*real_dy]

    def wall(self, dx, dy):
        """Check if we are hitting a wall"""
        # Initialize real_d- to proposed d- values
        real_dx = dx
        real_dy = dy

        # Compute where next positions are
        next_x = self.pos[0] + dx
        next_y = self.pos[1] + dy
        
        # Reset real_d- if we are going to go out of bounds
        if next_x < wall_pad or next_x > field_size - wall_pad:
            real_dx = 0
        if next_y < wall_pad or next_y > field_size - wall_pad:
            real_dy = 0

        return real_dx, real_dy

    def get_body(self):
        self.body = plt.Circle((self.pos[0],self.pos[1]), 
                        radius = self.weight, 
                        facecolor = self.color,
                        edgecolor = (0.0,0.0,0.0,0.0))
        return self.body

    def get_sparkle(self):
        data_left = self.sim.signals[self.sim.model.sig[self.antenna_left]['out']]
        data_right = self.sim.signals[self.sim.model.sig[self.antenna_right]['out']]
        data = np.append(data_left,data_right)
        dim = int(np.sqrt(self.my_neurons*2)) + 1
        square_data = np.reshape(np.pad(data, (0,dim**2 - len(data)), mode='constant'), (dim,dim))
        if (np.count_nonzero(square_data) > 0):
            dots = np.nonzero(square_data)
        else:
            dots = ([0],[0])
        return dots

    def devour(self, creatures, foods):
        for i in range(0, num_food):
            dist = np.linalg.norm(foods[i].pos - self.pos)
            if dist < (self.weight + food_size) and foods[i].exists and not foods[i].eaten:
                foods[i].eaten = True
                self.eat_count += food_size
                new_weight = self.weight + food_calories
                if new_weight > max_weight:
                    self.weight = max_weight
                else:
                    self.weight = new_weight
        for i in range(0, num_creatures):
            if self.id != i and not creatures[i].eaten:
                dist = np.linalg.norm(creatures[i].pos - self.pos)
                if dist < (self.weight + creatures[i].weight) and (self.weight > creatures[i].weight + pred_prey_buffer):
                    new_weight = self.weight + creatures[i].weight * creature_efficiency / 100
                    self.eat_count += creatures[i].weight * creature_efficiency / 100
                    creatures[i].eaten_count += 1
                    creatures[i].eaten = True
                    creatures[i].respawn_count = respawn_time
                    if new_weight > max_weight:
                        self.weight = max_weight
                    else:
                        self.weight = new_weight


    def get_antenna_line(self):
        if self.starved == False:
            x_array = []
            y_array = []
            for i in reversed(range(0, nodes_per_antenna)):
                x_array.append(self.left_tips[i][0])
                y_array.append(self.left_tips[i][1])
            x_array.append(self.pos[0])
            y_array.append(self.pos[1])
            for i in range(0, nodes_per_antenna):
                x_array.append(self.right_tips[i][0])
                y_array.append(self.right_tips[i][1])
            return [x_array, y_array] 
        elif self.starved == True:
            return self.pos
        
    def update_tips(self):
        left_tips = [[0,0] for i in range(0, nodes_per_antenna)]
        right_tips = [[0,0] for i in range(0, nodes_per_antenna)]
        for i in range(0, nodes_per_antenna):
            offset = antenna_length/nodes_per_antenna * (i+1)
            left_tips[i] = [(self.pos[0] + np.cos(self.angle+antennae_separation/2) * offset), 
                            (self.pos[1] + np.sin(self.angle+antennae_separation/2) * offset)]
            right_tips[i] = [(self.pos[0] + np.cos(self.angle-antennae_separation/2) * offset), 
                          (self.pos[1] + np.sin(self.angle-antennae_separation/2) * offset)]
        return left_tips, right_tips

    def respawn(self):
        self.respawn_count -= 1
        if self.respawn_count == 2:
            self.eaten = False
            self.starved = False
            self.color = tuple([ a + b for a, b in zip(list(creature_color),[float(self.id)/float(num_creatures), 0.0, -float(self.id)/float(num_creatures), 0.0])])
            self.vel = creature_velocity 
            self.pos = np.random.randint(wall_pad+2, field_size-2*wall_pad-2, size = 2)
            self.angle = np.random.uniform(0, 2*np.pi)
            self.weight = creature_weight
        if self.respawn_count < 1:
            self.respawning = True