import numpy as np
import matplotlib.pyplot as plt
from settings import *

class Food:
    """
    Class for all of the creatures
    """
    def __init__(self,
                 init_pos,
                 index
                 ):
        # Setup
        self.pos = np.asarray(init_pos, dtype=float)
        self.id = index
        self.exists = False
        self.eaten = False
        self.round = -1

    ##################
    # PHYSICS ENGINE #
    ##################

    def set_exists(self, t):
        """Decide if this food should exist"""
        round = t // (food_spawn_delay*num_food)
        if (t % (food_spawn_delay*num_food)//food_spawn_delay == self.id and self.round < round):
            self.eaten = False
            self.exists = True
            self.round = round

    def get_body(self):
        self.body = plt.Circle((self.pos[0],self.pos[1]), 
                        radius = food_size, 
                        facecolor = food_color,
                        edgecolor = food_color)
        return self.body
